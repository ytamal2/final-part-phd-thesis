ortho_table = read.xlsx("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Input_files/Additional_inputs/Orthologues_n_correspondence/ChirsutaOX_Athaliana_ORTHOLOGUES_NEW_With_RCO.xlsx")

ortho_table = ortho_table %>% mutate(C.hirsutaOX = gsub(pattern = "_T.*", replacement = "", x = C.hirsutaOX))  %>% 
  mutate(C.hirsutaOX = gsub(pattern = "_", replacement = "-", x = C.hirsutaOX))  %>% mutate(A.thaliana.TAIR10 = gsub(pattern = "\\..*", replacement = "", x = A.thaliana.TAIR10)) 

write.csv(ortho_table, file = "CH_AT_Ortho.csv", row.names = FALSE)


ortho_table = ortho_table %>% mutate(C.hirsutaOX = gsub(pattern = "_T.*", replacement = "", x = C.hirsutaOX))  %>% 
  mutate(A.thaliana.TAIR10 = gsub(pattern = "\\..*", replacement = "", x = A.thaliana.TAIR10)) 

write.csv(ortho_table, file = "Orthos_table.csv", row.names = FALSE)
