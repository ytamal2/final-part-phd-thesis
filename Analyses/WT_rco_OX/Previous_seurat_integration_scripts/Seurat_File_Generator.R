rm(list = ls())

# https://rdrr.io/github/MacoskoLab/liger/man/

##### Here loading all the packages and functions for this script to run ######
library(cowplot) # provides various features that help with creating publication-quality figures
library(ggplot2)
library(scExplorer)
library(liger)
library(ggpubr)
library(ggthemes)
library(ggsci)
library(scales)
library(Seurat)
library(openxlsx)
library(clustree)
library(tidyr)
library(ggthemes)
library(Matrix)
library(dplyr)
theme_set(theme_bw())
options(ggrepl.max.overlaps = Inf)

colPalettes = c(pal_rickandmorty("schwifty", alpha = 1)(12), pal_aaas(alpha = 1)(10), pal_uchicago()(9), pal_simpsons()(16))
mypal = colPalettes[-c(1,2,4,6,7,8,9,10,12,13,14,17,19,21,22,25,26,29,30,31,33,34,35,36,37,40,42,44)]
mypal = mypal[c(13, 2, 4, 11, 6, 12, 1, 9, 7, 5, 16, 19, 8, 14, 3, 10, 15, 17, 18)]
mypal = c(mypal, colPalettes[25])
mypal = mypal[c(20, 14, 4, 15, 9, 10, 19, 12, 16, 11, 8, 3, 1, 6, 2, 5, 7, 13, 17, 18)]
show_col(mypal)

dev.off()

# Functions
annotationGenerator <- function(geneNameFile, geneFamilyFile, yourOutputFile){
  
  ortho_table = read.csv("../../C.hirsuta_A.thaliana_ORTHOLOGUES.csv")
  ortho_table = ortho_table[ , 1:2]
  
  GName = read.delim(geneNameFile)
  GFamily = read.delim(geneFamilyFile)
  
  name_family_merged = merge(GName, GFamily, by = "geneID", all = TRUE)
  merge_chID = merge(name_family_merged, ortho_table, by.x = "geneID", by.y = "ath_gene")
  
  annotatedDF = merge_chID[, c(4,1,3,2)]
  write.xlsx(annotatedDF, yourOutputFile)
}

orthoFinderAT <- function(MarkersVector){
  orthologues = read.delim("../../../Input_Files/chirsutaOX.arabidopsisARAPORT.one2one.txt")
  orthologues$chirsutaOX <- gsub("_", "-", orthologues$chirsutaOX)
  genes_ortho = orthologues[orthologues[, 2] %in% MarkersVector, ]
  genes_ortho
}

orthoFinderCH <- function(MarkersVector){
  orthologues = read.delim("../../../Input_Files/chirsutaOX.arabidopsisARAPORT.one2one.txt")
  orthologues$chirsutaOX <- gsub("_", "-", orthologues$chirsutaOX)
  genes_ortho = orthologues[orthologues[, 1] %in% MarkersVector, ]
  genes_ortho
}

maxCountPerCell <- function(yourObject, numberOfGenes){
  mat = as.matrix(yourObject)
  max_genes = apply(mat, 2, max)
  sort(max_genes, decreasing = T)[1:numberOfGenes]
}

# Theme specification
themeSpec <- theme(axis.title.x = element_text(size = 34, face = "bold"), 
                   axis.title.y = element_text(size = 34, face = "bold"), 
                   axis.ticks.length = unit(.30, "cm"), 
                   axis.text = element_text(size = 34, face = "bold"),
                   legend.key.size = unit(4,"line"),
                   legend.key = element_rect(size = 34),
                   legend.text = element_text(size = 26, face = "bold"))

# Expression of the RCO gene
rcoEx <- function(seuratObject, feature, figureName){
  p <- VlnPlot(seuratObject, features = c("Chir06Ox-b38140.1"), slot = "counts", pt.size = 3, split.by = "genotype") + NoLegend() + 
    xlab("Cell Clusters") + 
    ggtitle("RCO") + 
    theme(axis.title.x = element_text(size = 24, face = "bold"), 
          axis.title.y = element_text(size = 24, face = "bold"), 
          axis.ticks.length = unit(.30, "cm"), 
          axis.text = element_text(size = 24, face = "bold")
    )
  ggsave(
    figureName,
    p,
    width = 24,
    height = 20,
    dpi = 300
  )
}

theme_set(theme_bw())

# 1st Step:
# Load the saved integrated file
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2021/Final_Analyses/Analyses/WT_RCO_OX_All_Experiments/Seurat_LogNorm_11_02_2022/OX_WT_RCO_LogNorm.RData")

OX1_OX3_OX7_OX2_RCO3_RCO7_04 <- integrated.data
rm(integrated.data)

gc()

OX1_OX3_OX7_OX2_RCO3_RCO7_04 <- RunTSNE(OX1_OX3_OX7_OX2_RCO3_RCO7_04, reduction = "pca", dims = 1:50)

DefaultAssay(OX1_OX3_OX7_OX2_RCO3_RCO7_04)
Idents(OX1_OX3_OX7_OX2_RCO3_RCO7_04)

# Lets get the list of Highly variable genes from the Seurat object
HVGs <- VariableFeatures(OX1_OX3_OX7_OX2_RCO3_RCO7_04)

paste("What is the active ident?", paste(levels(OX1_OX3_OX7_OX2_RCO3_RCO7_04@active.ident), collapse = ", "))

############################ Change this chunk ############################
OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4 <- factor(OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4, levels = seq(0, length(levels(OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4)) - 1))
Idents(OX1_OX3_OX7_OX2_RCO3_RCO7_04) <- OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4
OX1_OX3_OX7_OX2_RCO3_RCO7_04$seurat_clusters <- OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4

paste("What is the active ident?", paste(levels(OX1_OX3_OX7_OX2_RCO3_RCO7_04@active.ident), collapse = ", "))

Idents(OX1_OX3_OX7_OX2_RCO3_RCO7_04) <- factor(Idents(OX1_OX3_OX7_OX2_RCO3_RCO7_04), levels = seq(0, length(levels(OX1_OX3_OX7_OX2_RCO3_RCO7_04$integrated_snn_res.0.4)) - 1))
###########################################################################

# Clustering was performed for multiple choices of of the resolution parameter, and should be saved in the meta-data file
# In case, we want to plot the cells without cluster labels, set the idents to O or None

###### Change from here
OX1_OX3_OX7_OX2_RCO3_RCO7_04$orig.ident <- factor(OX1_OX3_OX7_OX2_RCO3_RCO7_04$orig.ident, levels = c("OX_2E", "OX_7E", "OX_1E", "OX_3E", "RCO_3E", "RCO_7E"))

OX1_OX3_OX7_OX2_RCO3_RCO7_04$stim <- OX1_OX3_OX7_OX2_RCO3_RCO7_04$orig.ident

OX1_OX3_OX7_OX2_RCO3_RCO7_04$stim <- factor(OX1_OX3_OX7_OX2_RCO3_RCO7_04$stim, levels = c("OX_2E", "OX_7E", "OX_1E", "OX_3E", "RCO_3E", "RCO_7E"), labels = c("Ox2", "Ox7", "Ox1", "Ox3", "rco3", "rco7"))

save(OX1_OX3_OX7_OX2_RCO3_RCO7_04, file = "OX1_OX3_OX7_OX2_RCO3_RCO7_04_without_protoplasting.RData")
