# Pick the colors

temp_sps = levels(integrated.data@meta.data[["Species"]])
temp_reps = levels(integrated.data@meta.data[["Replicates"]])

# set the color base
base_col = "#f2edee" # The base color
grp_col = c("#FD6467", "#00A08A", "#F98400", "#046C9A") # Manually picked colors for each species class
sps_col = grp_col[c(1:length(temp_sps))]

temp_col_list = list()

# Lets create color gradient for the replicates for each group of species
for (items in c(1:length(temp_sps))){
  temp_col = colorRampPalette(c(base_col, sps_col[items]))(length(grep(temp_sps[items], unique(str_c(integrated.data@meta.data[["Replicates"]], integrated.data@meta.data[["Species"]])))) + 2)[-c(1:2)]
  temp_col_list[[items]] = temp_col
}

rep_col = rapply(temp_col_list, c) # we can also use c(list, recursive = TRUE) or unlist(list = use.names = FALSE)

# For any gene ID, this script will find total number of cells in the cell atlas expressing that gene
# STM = "AT1G62360"

interested_gene = "AT1G62360"

# Proportion check

# This part checks the number of stm expressing cells in cluster 13 where majority of the STM expressing cells are grouped together - visualized per replicate
count_n_cluster = merge(total_cell_prop[ , c("species", "total_cell_count")], cell_prop, by = "species")

count_n_cluster = count_n_cluster %>% dplyr::select(species, cluster, cell_count, total_cell_count) %>% 
  dplyr::mutate(frac_detected = round((cell_count / total_cell_count) * 100, 1)) %>%
  mutate(frac_label = str_c(frac_detected, "%")) %>%
  mutate(count_label = str_c(cell_count, frac_label, sep = ",\n")) %>%
  mutate_at(vars(species), as.factor)

# Lets get the cell proportion visualization
p <- ggplot(data = count_n_cluster, aes(x = cluster, y = frac_detected, fill = species)) + 
  geom_bar(stat = "identity", position = "dodge", width = 0.8) +
  geom_text(aes(x = cluster, y = frac_detected, label = frac_label),  position = position_dodge(width = 0.8), size = 3, vjust = -0.5, fontface = "bold") + 
  xlab("Cell clusters") + ylab("Number of cells") + 
  theme(
    panel.border = element_blank(),
    axis.line = element_line(colour = "#71D0F5FF"),
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.background = element_blank(), # Background of the entire plot
    axis.title = element_text(size = 26, face = "bold", color = "black"),
    axis.ticks.length = unit(.20, "cm"), 
    axis.text = element_text(size = 26, face = "bold", colour = "black"),
    legend.title = element_blank(),
    legend.key.size = unit(2, "line"), 
    legend.text = element_text(size = 22, face = "bold")) + 
  guides(colour = guide_legend(override.aes = list(size = 8))) + 
  scale_fill_manual(values = sps_col) + guides(fill = guide_legend(title = "Species"), color = guide_legend(override.aes = list(size = 8)))
p



ggbasicSaver(p, "STM_expressing_cells_per_species_proportion_in_cluster_13.png", 6, 10)


