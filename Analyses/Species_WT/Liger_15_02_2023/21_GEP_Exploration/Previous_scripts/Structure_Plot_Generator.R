## Generate heatmap
df <- df_H

df <- data.frame(Cells = rownames(df), df)
df$Cells <- factor(df$Cells, levels = df$Cells)

mat <- melt(df, id.vars = "Cells", variable.name = "GEPs", value.name = "Expression")
mat <- mat %>% arrange(-Expression)

p <- ggplot(mat, aes(x = Cells, y = Expression, fill = GEPs)) + geom_bar(stat = "identity") + xlab("Cells") + ylab("Expression") + 
  theme(
    panel.border = element_blank(),
    axis.line = element_blank(),
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.background = element_blank(), # Background of the entire plot
    axis.title = element_text(size = 26, face = "bold", color = "black"),
    axis.ticks.length = unit(.20, "cm"), 
    axis.ticks.x = element_blank(),
    axis.text.y = element_text(size = 20, face = "bold", colour = "black"),
    axis.text.x = element_blank(),
    legend.title = element_text(size = 20, face = "bold", colour = "black"),
    legend.key.size = unit(2, "line"), 
    legend.text = element_text(size = 20, face = "bold"))
ggbasicSaver(p, "Stacked_bar.png", 32, 16)


#### Structure plot for cells per cluster
### Structure plot of the cells per cluster
temp = data.frame(Cells = rownames(cell_clusters1), Clusters = cell_clusters1[,1], row.names = rownames(cell_clusters1))
temp2 <- coef1[rownames(temp[temp$Clusters == "1", ]), ]

mat = coef1[rownames(temp2), ]
mat = mat[order(mat$GEP_1, decreasing = TRUE), ]
mat_annotation = annotation
rownames(mat_annotation) = mat_annotation$sample_id
mat_annotation = mat_annotation[rownames(mat), ]

p1 <- StructureGGplot(omega = mat,
                      annotation = mat_annotation,
                      palette = mypal,
                      yaxis_label = "Cell Clusters",
                      order_sample = TRUE,
                      sample_order_decreasing = T,
                      legend_title_size = 16,
                      legend_key_size = 0.6,
                      legend_text_size = 16) + 
  ylab("Membership proportion") + 
  xlab("Cell clusters") + 
  theme(axis.title.x = element_text(size = 16, face = "bold", colour = "black"), 
        axis.title.y = element_text(size = 16, face = "bold", colour = "black"), 
        axis.ticks.length = unit(.30, "cm"), 
        axis.text = element_text(size = 12, face = "bold", colour = "black"),
        legend.key.size = unit(2,"line"), 
        legend.text = element_text(size = 18, face = "bold"),
        legend.box = "vertical") + 
  guides(fill = guide_legend(ncol = 1, override.aes = list(size = 4)))

ggbasicSaver(p1, "StructurePlot_C1.png", 28, 32)
