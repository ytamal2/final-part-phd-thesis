# Load all the functions stored in scripts from the folder housing the scripts
scripts_list <- list.files("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions", pattern = "*.R$", full.names = TRUE) 
sapply(scripts_list, source, .GlobalEnv)

###############################

# let's load a seurat object
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects_with_scaled_coefficient/seurat_object_of_K50.RData")

############################ Change this chunk ############################
integrated.data$RNA_snn_res.0.4 <- factor(integrated.data$RNA_snn_res.0.4, levels = seq(0, length(levels(integrated.data$RNA_snn_res.0.4)) - 1))
Idents(integrated.data) <- integrated.data$RNA_snn_res.0.4

paste("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", "))

Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
###########################################################################


# Let's get the coefficient matrix
coef = as.data.frame(integrated.data@reductions[["inmf"]]@cell.embeddings)
colnames(coef) = str_c("GEP_", c(1:50))

a = coef$GEP_1
b = log1p(a)
integrated.data$GEP1 = integrated.data@reductions$inmf@cell.embeddings[, 26]

integrated.data$GEP1 = (integrated.data$GEP1)*10

FeaturePlot(integrated.data, features = GEP1, reduction = "umap", dims = c(1, 2), pt.size = 0.5, order = T, min.cutoff = 0.001, cols = c("grey", RColorBrewer::brewer.pal(9, "Reds")[8])) +
  ggtitle(str_c("Factor", " - " , 1)) + labs(color='Loadings') + 
  theme(
    axis.title = element_text(size = 18, face = "bold"),
    axis.ticks.length = unit(.30, "cm"), 
    axis.text = element_text(size = 18, face = "bold"),
    title = element_text(size = 24, face = "bold"),
    legend.key.size = unit(2,"line"),
    legend.key = element_rect(size = 20),
    legend.text = element_text(size = 18, face = "bold"))

ggsave(filename = str_c(temp_dir, genes_name, "_expression_across_cells_", toupper(reduction_name), "_dims_", paste(dims_to_plot, collapse = "_"), figure_name_pref, ".png"), plot = p, width = 14, height = 14, dpi = 300, bg = "transparent")

FeaturePlot(integrated.data, features = integrated.data@reductions$inmf@cell.embeddings[, 26], reduction = "umap")

map_factor_loadings(seuratObject = integrated.data, store_dir = getwd(), factor_ids = c(26, 50, 45), factor_to_plot = "inmf", reduction_name = "umap")
