# Load all the functions stored in scripts from the folder housing the scripts
scripts_list <- list.files("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions", pattern = "*.R$", full.names = TRUE) 
sapply(scripts_list, source, .GlobalEnv)

###############################

# let's load a seurat object
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects_with_scaled_coefficient/seurat_object_of_K50.RData")

############################ Change this chunk ############################
integrated.data$RNA_snn_res.0.4 <- factor(integrated.data$RNA_snn_res.0.4, levels = seq(0, length(levels(integrated.data$RNA_snn_res.0.4)) - 1))
Idents(integrated.data) <- integrated.data$RNA_snn_res.0.4

paste("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", "))

Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
###########################################################################

rco_subset <- subset(integrated.data, idents = c("9", "11"))

RDimension_plot(seuratObject = rco_subset, store_dir = getwd(), store_folder = NULL, figure_name_pref = "_RCO_cell_clusters")

rco_subset[["initial.clustering"]] <- rco_subset[["RNA_snn_res.0.4"]]

rco_subset[["initial.clustering"]] <- droplevels(rco_subset[["initial.clustering"]])

meta_remove <- grep(x = names(rco_subset@meta.data), pattern = "RNA_snn_res*", value = TRUE)

for (i in meta_remove) {
  rco_subset[[i]] <- NULL
}

# Let's split the object based on dataset information
rco_data <- SplitObject(rco_subset, split.by = "Datasets")

rco_data <- lapply(X = rco_data, FUN = SCTransform)

features <- SelectIntegrationFeatures(object.list = rco_data, nfeatures = 3000)

rco_data <- PrepSCTIntegration(object.list = rco_data, anchor.features = features)

# Find integration anchors
rco.anchors <- FindIntegrationAnchors(object.list = rco_data, normalization.method = "SCT", anchor.features = features)

# Integrate the data
integrated.rco <- IntegrateData(anchorset = rco.anchors, normalization.method = "SCT", k.weight = 30)

integrated.rco <- RunPCA(integrated.rco, features = features)

ggsave(plot = ElbowPlot(integrated.rco, ndims = 50), filename = "Elbow.png", width = 12, height = 12, bg = "white")

PC_to_test = c(5:50)

for (i in c(1:length(PC_to_test))){
  
  dir.create(str_c("N_PC_", PC_to_test[i]), showWarnings = TRUE, recursive = FALSE, mode = "0777")
  
  temp_dir = str_c("N_PC_", PC_to_test[i], "/")
  
  DefaultAssay(integrated.rco) <- "integrated"
  
  integrated.rco <- RunUMAP(integrated.rco, reduction = "pca", dims = 1:PC_to_test[i], n.components = 2)
  
  integrated.rco <- FindNeighbors(integrated.rco, dims = 1:PC_to_test[i], reduction = "pca")
  
  integrated.rco <- FindClusters(integrated.rco, resolution = seq(0.1, 1, 0.1), n.iter = 50, n.start = 50)
  
  cl_idents = grep(x = names(integrated.rco@meta.data), pattern = "integrated_snn_res*", value = TRUE)
  
  for (j in c(1:length(cl_idents))){
    
    temp_str = substr(gsub("[^[:alnum:]]", "", cl_idents[j]), start = nchar(gsub("[^[:alnum:]]", "", cl_idents[j])) - 1, stop = nchar(gsub("[^[:alnum:]]", "", cl_idents[j])))
    temp_str = str_c("RES_", gsub(pattern = "[A-Za-z]", replacement = "", temp_str))
    
    dir.create(str_c("N_PC_", PC_to_test[i], "/", temp_str), showWarnings = TRUE, recursive = FALSE, mode = "0777")
    
    temp_res_dir = str_c(str_c("N_PC_", PC_to_test[i], "/", temp_str, "/"))
    
    Idents(integrated.rco) <- cl_idents[j]
    
    DefaultAssay(integrated.rco) <- "RNA"
    
    RDimension_plot(seuratObject = integrated.rco, store_dir = temp_res_dir, store_folder = NULL)
    
    genes_feature_plot(seuratObject = integrated.rco, store_dir = temp_res_dir, store_folder = NULL, genes_ID = "AT5G67651", genes_name = "RCO")
    
    feature_count_n_proportion(seuratObject = integrated.rco, store_dir = temp_res_dir, store_folder = NULL, gene_ID = "AT5G67651", gene_name = "RCO")
    
  }
} 
