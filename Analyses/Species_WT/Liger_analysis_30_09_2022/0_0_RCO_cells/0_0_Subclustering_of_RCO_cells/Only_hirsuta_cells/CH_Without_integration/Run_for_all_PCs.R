# Load all the functions stored in scripts from the folder housing the scripts
scripts_list <- list.files("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions", pattern = "*.R$", full.names = TRUE) 
sapply(scripts_list, source, .GlobalEnv)

###############################

# let's load a seurat object
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects_with_scaled_coefficient/seurat_object_of_K50.RData")

############################ Change this chunk ############################
integrated.data$RNA_snn_res.0.4 <- factor(integrated.data$RNA_snn_res.0.4, levels = seq(0, length(levels(integrated.data$RNA_snn_res.0.4)) - 1))
Idents(integrated.data) <- integrated.data$RNA_snn_res.0.4

paste("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", "))

Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
###########################################################################

reps_to_keep = c("OX-1", "OX-2", "OX-3", "OX-7")

rco_subset <- subset(integrated.data, idents = c("9", "11"), subset = Datasets %in% reps_to_keep)

for (i in names(rco_subset@meta.data)) {
  rco_subset[[i]] <- droplevels(rco_subset[[i]])
}

RDimension_plot(seuratObject = rco_subset)

rco_subset[["initial.clustering"]] <- rco_subset[["RNA_snn_res.0.4"]]

rco_subset[["initial.clustering"]] <- droplevels(rco_subset[["initial.clustering"]])

meta_remove <- grep(x = names(rco_subset@meta.data), pattern = "RNA_snn_res*", value = TRUE)

for (i in meta_remove) {
  rco_subset[[i]] <- NULL
}

rco_subset <- ScaleData(rco_subset)

rco_subset <- RunPCA(rco_subset)

PC_to_test = c(5:50)

ggsave(plot = ElbowPlot(rco_subset, ndims = 50), filename = "Elbow.png", width = 12, height = 12, bg = "white")

for (i in c(1:length(PC_to_test))){
  
  dir.create(str_c("N_PC_", PC_to_test[i]), showWarnings = TRUE, recursive = FALSE, mode = "0777")
  
  temp_dir = str_c("N_PC_", PC_to_test[i], "/")
  
  rco_subset <- RunUMAP(rco_subset, reduction = "pca", dims = 1:PC_to_test[i], n.components = 2)
  
  rco_subset <- FindNeighbors(rco_subset, dims = 1:PC_to_test[i], reduction = "pca")
  
  rco_subset <- FindClusters(rco_subset, resolution = seq(0.1, 1, 0.1), n.iter = 50, n.start = 50)
  
  cl_idents = grep(x = names(rco_subset@meta.data), pattern = "integrated_snn_res*", value = TRUE)
  
  for (j in c(1:length(cl_idents))){
    
    temp_str = substr(gsub("[^[:alnum:]]", "", cl_idents[j]), start = nchar(gsub("[^[:alnum:]]", "", cl_idents[j])) - 1, stop = nchar(gsub("[^[:alnum:]]", "", cl_idents[j])))
    temp_str = str_c("RES_", gsub(pattern = "[A-Za-z]", replacement = "", temp_str))
    
    dir.create(str_c("N_PC_", PC_to_test[i], "/", temp_str), showWarnings = TRUE, recursive = FALSE, mode = "0777")
    
    temp_res_dir = str_c(str_c("N_PC_", PC_to_test[i], "/", temp_str, "/"))
    
    Idents(rco_subset) <- cl_idents[j]
    
    RDimension_plot(seuratObject = rco_subset, store_dir = temp_res_dir, store_folder = NULL)
    
    genes_feature_plot(seuratObject = rco_subset, store_dir = temp_res_dir, store_folder = NULL, genes_ID = "AT5G67651", genes_name = "RCO")
    
    feature_count_n_proportion(seuratObject = rco_subset, store_dir = temp_res_dir, store_folder = NULL, gene_ID = "AT5G67651", gene_name = "RCO")
    
  }
} 

