# Load all the functions stored in scripts from the folder housing the scripts
scripts_list <- list.files("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions", pattern = "*.R$", full.names = TRUE) 
sapply(scripts_list, source, .GlobalEnv)

###############################

# let's load a seurat object
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects_with_scaled_coefficient/seurat_object_of_K50.RData")

############################ Change this chunk ############################
integrated.data$RNA_snn_res.0.4 <- factor(integrated.data$RNA_snn_res.0.4, levels = seq(0, length(levels(integrated.data$RNA_snn_res.0.4)) - 1))
Idents(integrated.data) <- integrated.data$RNA_snn_res.0.4

paste("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", "))

Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
###########################################################################

# Let's get the coefficient matrix
coef = as.data.frame(integrated.data@reductions[["inmf"]]@cell.embeddings)
colnames(coef) = str_c("GEP_", c(1:50))

coef = order_factorized_matrix(coef)

cells_order = c()

for (i in levels(integrated.data)){
  cell_idents = WhichCells(integrated.data, idents = i)
  cells_order = c(cells_order, cell_idents)
}

# rco cluster cells
rco_cluster_cells = WhichCells(integrated.data, idents = c(9, 11))

mat_H = coef[cells_order, ]

mat_H = data.frame(Cells = rownames(mat_H), mat_H)
mat_H$Cells = factor(mat_H$Cells, levels = mat_H$Cells)


mat_H <-
  mat_H %>% melt(
    id.vars = "Cells",
    variable.name = "GEPs",
    value.name = "Expression"
  )

intercepts = c()

for (i in levels(integrated.data)) {intercepts = c(intercepts, length(WhichCells(integrated.data, idents = i)))}

intercepts = cumsum(intercepts)[-23]

p <-
  ggplot(mat_H, aes(x = Cells, y = GEPs)) + geom_tile(aes(fill = Expression)) +
  scale_fill_gradient(
    name = "Other cells",
    low = "#F0FFFF",
    high = RColorBrewer::brewer.pal(9, "Blues")[8],
    limit = c(min(mat_H$Expression), 1),
    space = "Lab",
    guide = "colourbar"
  ) +
  new_scale("fill") +
  geom_tile(aes(fill = Expression), data = mat_H[which(mat_H$Cells %in% rco_cluster_cells),]) +
  scale_fill_gradient(
    name = "Cells of interest",
    low = "#F0FFFF",
    high = RColorBrewer::brewer.pal(9, "Reds")[8],
    limit = c(min(mat_H$Expression), 1),
    space = "Lab",
    guide = "colourbar"
  ) +
  geom_vline(xintercept = intercepts, linetype = "dotted", face = "bold") + 
  xlab("Cells") +
  theme(
    panel.border = element_blank(),
    axis.line = element_blank(),
    panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.background = element_blank(),
    axis.title = element_text(size = 32, face = "bold", color = "black"),
    axis.ticks.length = unit(.20, "cm"),
    axis.ticks.x = element_blank(),
    axis.text.y = element_text(
      size = 24,
      face = "bold",
    ),
    axis.text.x = element_blank(),
    legend.title = element_text(
      size = 24,
      face = "bold",
      colour = "black"
    ),
    legend.key.size = unit(3, "line"),
    legend.text = element_text(size = 24, face = "bold")
  )
ggsave(
  filename = "Trial.png",
  plot = p,
  width = 44,
  height = 28,
  dpi = 300
)

heatmap_cells_coefficient(seuratObject = integrated.data, store_dir = getwd(), cell_ids = WhichCells(integrated.data, idents = "21"), figureName = "STM_cluster")
heatmap_cells_coefficient(seuratObject = integrated.data, store_dir = getwd(), cell_ids = WhichCells(integrated.data, idents = "9"), figureName = "RCO_cluster_9")
heatmap_cells_coefficient(seuratObject = integrated.data, store_dir = getwd(), cell_ids = WhichCells(integrated.data, idents = "11"), figureName = "RCO_cluster_11")
# heatmap_cells_coefficient(seuratObject = integrated.data, store_dir = getwd(), cell_ids = WhichCells(integrated.data, idents = "9", expression = AT5G67651 > 0), figureName = "rco_expressing_cells_cluster_9")
