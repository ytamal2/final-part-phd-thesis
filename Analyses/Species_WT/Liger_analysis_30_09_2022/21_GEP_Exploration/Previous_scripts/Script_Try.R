numPeople = 10
sex = sample(c("male", "female"), numPeople, replace = T)
age = sample(14:102, numPeople, replace = T)
income = sample(20:150, numPeople, replace = T)
minor = age < 18


population = data.frame(
  sex = sex,
  age = age,
  income = income,
  minor = minor
)

# does not work
df_sorted <- population[order(population$age),]
df_sorted_multiple <- population[
  with(population, order(age, income)),
]

df <- (data.frame(matrix(data=sort(runif(10000)),ncol = 1000)))


df_sorted <- df[order(apply(df, 1, function(x) {which(as.numeric(x) == max(as.numeric(x)))})),]
# rownames(df_sorted) <- seq(1, nrow(df_sorted))

df_matrix <- as.matrix.data.frame(df_sorted)

image(df_matrix)

# What does it do?
df_sorted <- df[order(apply(df, 1, function(x) {which.max(x)})),]
# This ordering finds the index of max expression value for each cell, then order the index value, and sort the df using this order

# Lets try this one - This worked
# This will sort the dataframe by columns in sequence and expression values in decreasing order
df <- coef1
df_list = list()
for (i in(c(1:ncol(df)))){
  temp = df[apply(df, 1, function(x) {which.max(x)}) == i,]
  temp = temp[order(temp[, i], decreasing = TRUE), ]
  df_list[[i]] <- temp
}

df <- do.call(rbind.data.frame, df_list)

