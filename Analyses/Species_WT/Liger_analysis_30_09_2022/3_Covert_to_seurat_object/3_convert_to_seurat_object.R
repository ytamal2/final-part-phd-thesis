source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/Load_libraries.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/load_rdata.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/liger_to_seurat_conversion.R")

###
# Convert the liger object to a seurat object to perform downstream analysis
###

storing_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022"

liger_to_seurat_conversion(liger_object_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Liger_objects", 
                           file_name_pattern = "Liger_object_K_",
                           store_dir = storing_dir,
                           store_folder = "Seurat_objects")
