source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/Load_libraries.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/load_rdata.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/replicate_proportion_per_cluster.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/species_proportion_per_cluster.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/feature_count_n_proportion.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/genes_feature_plot.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/resolution_explorer.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/resolution_explorer_series_of_seurat_objects.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/markers_dot_plot.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/side_by_side_proportion_comparison.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/cluster_conserved_marker_finder.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/between_groups_differentially_expressed_genes.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/group_n_cluster_DEG_finder.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/cluster_marker_finder.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/cells_per_cluster_split_viz.R")

# Storing directory
# res_dir = "/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Analyses/Species_WT/Liger_analysis_30_09_2022/Temp_results/"

load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects_with_scaled_coefficient/seurat_object_of_K50.RData")

Idents(integrated.data) <- "RNA_snn_res.0.4"

cells_per_cluster_split_viz(seuratObject = integrated.data, store_dir = getwd(), split_variable = "Species")
