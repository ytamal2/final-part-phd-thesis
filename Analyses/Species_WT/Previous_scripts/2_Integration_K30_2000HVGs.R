rm(list = ls())

# https://rdrr.io/github/MacoskoLab/liger/man/

##### Here loading all the packages and functions for this script to run ######
library(cowplot) # provides various features that help with creating publication-quality figures
library(ggplot2)
library(scExplorer)
library(rliger)
library(ggpubr)
library(ggthemes)
library(ggsci)
library(scales)
library(Seurat)
library(openxlsx)
library(clustree)
library(tidyr)
library(ggthemes)
library(Matrix)
library(dplyr)
library(tidyverse)
theme_set(theme_bw())
options(ggrepl.max.overlaps = Inf)

colPalettes = c(pal_rickandmorty("schwifty", alpha = 1)(12), pal_aaas(alpha = 1)(10), pal_uchicago()(9), pal_simpsons()(16))
mypal = colPalettes[-c(1,2,4,6,7,8,9,10,12,13,14,17,19,21,22,25,26,29,30,31,33,34,35,36,37,40,42,44)]
mypal = mypal[c(13, 2, 4, 11, 6, 12, 1, 9, 7, 5, 16, 19, 8, 14, 3, 10, 15, 17, 18)]
mypal = c(mypal, colPalettes[25])
mypal = mypal[c(20, 14, 4, 15, 9, 10, 19, 12, 16, 11, 8, 3, 1, 6, 2, 5, 7, 13, 17, 18)]
show_col(mypal)

dev.off()

# Functions
annotationGenerator <- function(geneNameFile, geneFamilyFile, yourOutputFile){
  
  ortho_table = read.csv("../../../Input_Files/chirsutaOX.arabidopsisTAIR10.one2one.txt")
  ortho_table = ortho_table[ , 1:2]
  
  GName = read.delim(geneNameFile)
  GFamily = read.delim(geneFamilyFile)
  
  name_family_merged = merge(GName, GFamily, by = "geneID", all = TRUE)
  merge_chID = merge(name_family_merged, ortho_table, by.x = "geneID", by.y = "ath_gene")
  
  annotatedDF = merge_chID[, c(4,1,3,2)]
  write.xlsx(annotatedDF, yourOutputFile)
}

orthoFinderAT <- function(MarkersVector){
  orthologues = read.delim("../../../Input_Files/chirsutaOX.arabidopsisTAIR10.one2one.txt")
  orthologues$chirsutaOX <- gsub("_", "-", orthologues$chirsutaOX)
  genes_ortho = orthologues[orthologues[, 2] %in% MarkersVector, ]
  genes_ortho
}

orthoFinderCH <- function(MarkersVector){
  orthologues = read.delim("../../../Input_Files/chirsutaOX.arabidopsisTAIR10.one2one.txt")
  orthologues$chirsutaOX <- gsub("_", "-", orthologues$chirsutaOX)
  genes_ortho = orthologues[orthologues[, 1] %in% MarkersVector, ]
  genes_ortho
}

maxCountPerCell <- function(yourObject, numberOfGenes){
  mat = as.matrix(yourObject)
  max_genes = apply(mat, 2, max)
  sort(max_genes, decreasing = T)[1:numberOfGenes]
}

# Theme specification
themeSpec <- theme(axis.title.x = element_text(size = 34, face = "bold"), 
                   axis.title.y = element_text(size = 34, face = "bold"), 
                   axis.ticks.length = unit(.30, "cm"), 
                   axis.text = element_text(size = 34, face = "bold"),
                   legend.key.size = unit(4,"line"),
                   legend.key = element_rect(size = 34),
                   legend.text = element_text(size = 26, face = "bold"))

# Expression of the RCO gene
rcoEx <- function(seuratObject, feature, figureName){
  p <- VlnPlot(seuratObject, features = c("Chir06Ox-b38140.1"), slot = "counts", pt.size = 3, split.by = "genotype") + NoLegend() + 
    xlab("Cell Clusters") + 
    ggtitle("RCO") + 
    theme(axis.title.x = element_text(size = 24, face = "bold"), 
          axis.title.y = element_text(size = 24, face = "bold"), 
          axis.ticks.length = unit(.30, "cm"), 
          axis.text = element_text(size = 24, face = "bold")
    )
  ggsave(
    figureName,
    p,
    width = 24,
    height = 20,
    dpi = 300
  )
}

theme_set(theme_bw())

###############################

### Orthologues list 

# Reading the orthologues table
OrthoGenes = read.delim("../../../Input_Files/chirsutaOX.arabidopsisTAIR10.one2one.txt")
# OrthoGenes$chirsutaOX = gsub(pattern = "\\..*", replacement = "", OrthoGenes$chirsutaOX)
OrthoGenes$athaliana_tair10 = gsub(pattern = "\\..*", replacement = "", OrthoGenes$athaliana_tair10)

RCOgene <- c("Chir06Ox_b38140.1", "AT5G67651")
OrthoGenes = rbind(OrthoGenes, RCOgene)

# Sorting the ortho table based on Cardamine genes
OrthoGenes <- OrthoGenes[order(OrthoGenes$athaliana_tair10), ]

# Setting the rownames by Cardamine IDs
rownames(OrthoGenes) <- OrthoGenes$athaliana_tair10

##### Load the protoplasting induced genes list

# Loading the latest protoplasted induced genes list 
PP_genes = read.csv("../../../Input_Files/protoplasting_genes_Col0_Ox_ORTHO.csv")

# Assign cardamine IDs as the row names
rownames(PP_genes) = PP_genes$GeneID

# cell cycle genes

# Lets get the protoplasted induced genes names and cell-cycle gene names
PPgenes = as.character(rownames(PP_genes))

genesTOexclude = unique(c(PPgenes))

#### To keep only those ids

################################### WT Thaliana datasets #################################

# WT COL data 5th Experiment - leaf 5 and 6
COL_data_5E <- Read10X(data.dir = "../../../Input_Files/outs_Col0_RNA_5th_ALL_2/filtered_feature_bc_matrix/")

# WT COL data 1st Experiment - leaf 5 and 6
COL_data_1E <- Read10X(data.dir = "../../../Input_Files/outs_Col0_RNA_1ST_2/filtered_feature_bc_matrix/")

# WT COL data 3rd Experiment - leaf 5 and 6
COL_data_3E <- Read10X(data.dir = "../../../Input_Files/outs_Col0_RNA_3rd_ALL/filtered_feature_bc_matrix/")

# WT OX data 2nd Experiment - leaf 6 and 7
COL_data_2E <- Read10X(data.dir = "../../../Input_Files/outs_Col_RNA_2nd_ALL_2/filtered_feature_bc_matrix/")

# rco OX data 7th Experiment - leaf 6 and 7
COL_data_7E <- Read10X(data.dir = "../../../Input_Files/outs_Col_RNA_7th_ALL_2/filtered_feature_bc_matrix/")

##### Lets get the gene names
ATgenes = rownames(COL_data_1E)

# to do that we need to get the overlapping list of genes between the hirsuta dataset and orthologues list
OrthoGenesAT <- intersect(ATgenes, OrthoGenes$athaliana_tair10)

missing = setdiff(OrthoGenes$athaliana_tair10, toupper(ATgenes))
fileGenerator(missing, fileName = "MissingGenes_ThalaianData.txt")

# Out of the 19370 genes 19353 thaliana genes present in the ortho table

# Lets subset the orthologues table to keep only those genes that are present in thaliana datasets. This way we will not include hirsuta orthologues genes that are only 
# presnet in hirsuta and not thaliana.

OrthoGenes = OrthoGenes[OrthoGenesAT, ]
OrthoGenes = OrthoGenes[order(OrthoGenes$chirsutaOX), ]
rownames(OrthoGenes) = OrthoGenes$chirsutaOX

# extracting the Hirsuta IDs that are present in orthologue table 
OrthoGenesAT = as.character(OrthoGenes$athaliana_tair10)

# normally, we would expect that all the thaliana genes in the thaliana datasets should be present in the orthologues table, but it's not the case.
# There are a total of 17 thaliana genes that are present in the Ortho table for which we have homologus cardamine genes, 
# but surprisingly they are not present in the thaliana datasets.

#### Lets extract the dataset with the orthologues genes

COL_data_1E <- COL_data_1E[OrthoGenesAT, ]
COL_data_2E <- COL_data_2E[OrthoGenesAT, ]
COL_data_3E <- COL_data_3E[OrthoGenesAT, ]
COL_data_5E <- COL_data_5E[OrthoGenesAT, ]
COL_data_7E <- COL_data_7E[OrthoGenesAT, ]

# So, with this we only kept the orthologues ids in the thaliana datasets. Now, lets remove the genes induced by protoplasting
genesTOkeep = as.character(setdiff(OrthoGenesAT, genesTOexclude))

###### 
COL_data_1E <- COL_data_1E[genesTOkeep, ]
COL_data_3E <- COL_data_3E[genesTOkeep, ]
COL_data_5E <- COL_data_5E[genesTOkeep, ]

COL_data_2E <- COL_data_2E[genesTOkeep, ]
COL_data_7E <- COL_data_7E[genesTOkeep, ]

#### WT
# Leaf 5 & 6
COL_1E <- CreateSeuratObject(counts = COL_data_1E, project = "COL_1E", min.cells = 10, min.features = 200)
COL_1E <- subset(COL_1E, subset = nCount_RNA < 110000)
COL_1E
COL_W1 <- GetAssayData(COL_1E, assay = "RNA", slot = "counts")
colnames(COL_W1) <- paste("C1", colnames(COL_W1), sep = "_")

COL_3E <- CreateSeuratObject(counts = COL_data_3E, project = "COL_3E", min.cells = 6, min.features = 200)
COL_3E <- subset(COL_3E, subset = nCount_RNA < 110000)
COL_3E
COL_W3 <- GetAssayData(COL_3E, assay = "RNA", slot = "counts")
colnames(COL_W3) <- paste("C3", colnames(COL_W3), sep = "_")

COL_5E <- CreateSeuratObject(counts = COL_data_5E, project = "COL_5E", min.cells = 17, min.features = 200)
COL_5E <- subset(COL_5E, subset = nCount_RNA < 110000)
COL_5E
COL_W5 <- GetAssayData(COL_5E, assay = "RNA", slot = "counts")
colnames(COL_W5) <- paste("C5", colnames(COL_W5), sep = "_")

# Leaf 6 and 7
COL_2E <- CreateSeuratObject(counts = COL_data_2E, project = "COL_2E", min.cells = 27, min.features = 200)
COL_2E <- subset(COL_2E, subset = nCount_RNA < 110000)
COL_2E
COL_W2 <- GetAssayData(COL_2E, assay = "RNA", slot = "counts")
colnames(COL_W2) <- paste("C2", colnames(COL_W2), sep = "_")

# Previously I did run the analysis with nFeature_RNA < 10000 & nCount_RNA < 110000
COL_7E <- CreateSeuratObject(counts = COL_data_7E, project = "COL_7E", min.cells = 14, min.features = 200)
COL_7E <- subset(COL_7E, subset = nCount_RNA < 110000)
COL_7E
COL_W7 <- GetAssayData(COL_7E, assay = "RNA", slot = "counts")
colnames(COL_W7) <- paste("C7", colnames(COL_W7), sep = "_")

# Dimensions of thaliana datasets
dim(COL_W1)
dim(COL_W3)
dim(COL_W5)
dim(COL_W2)
dim(COL_W7)

################################### WT Hirsuta datasets #################################
# WT OX data 2nd Experiment - leaf 6 and 7
OX_data_2E <- Read10X(data.dir = "../../../Input_Files/outs_Ox_RNA_2nd_ALL_2_New/filtered_feature_bc_matrix/")

# rco OX data 7th Experiment - leaf 6 and 7
OX_data_7E <- Read10X(data.dir = "../../../Input_Files/outs_Ox_RNA_7th_ALL_2_New/filtered_feature_bc_matrix/")

# WT OX data 1st Experiment - leaf 5 and 6
OX_data_1E <- Read10X(data.dir = "../../../Input_Files/outs_OX_RNA_1ST_2_New/filtered_feature_bc_matrix/")

# WT OX data 3rd Experiment - leaf 5 and 6
OX_data_3E <- Read10X(data.dir = "../../../Input_Files/outs_Ox_RNA_3rd_ALL_3000_New/filtered_feature_bc_matrix/")

# Lets get the total set of genes of  Cardamine
CHgenes = rownames(OX_data_2E)

# extracting the Cardamine IDs that are present in orthologues table 
OrthoGenesCH = as.character(OrthoGenes$chirsutaOX)

dim(OX_data_1E)
dim(OX_data_2E)
dim(OX_data_3E)
dim(OX_data_7E)

#### Lets extract the dataset with the orthologues genes

# to do that we need to get the overlapping list of genes between the hirsuta dataset and orthologues list
OrthoGenesCH <- intersect(CHgenes, OrthoGenesCH)

OX_data_2E <- OX_data_2E[OrthoGenesCH, ]
OX_data_7E <- OX_data_7E[OrthoGenesCH, ]

OX_data_1E <- OX_data_1E[OrthoGenesCH, ]
OX_data_3E <- OX_data_3E[OrthoGenesCH, ]


# Ortho table for the hirsuta ids
OL_CH <- OrthoGenes

# Drop the gene ID columns from the dataframe
drops <- c("CHID", "athaliana_tair10") ## Columns to drop

OX_DF_1 <- as.data.frame(OX_data_1E)
OX_DF_1$CHID <- rownames(OX_DF_1)
OX_DF_1 <- merge(x = OX_DF_1, y = OL_CH, by.x = "CHID", by.y = "chirsutaOX", all.x = T)
rownames(OX_DF_1) <- OX_DF_1$athaliana_tair10
OX_DF_1 <- as.matrix(OX_DF_1[ ,!(names(OX_DF_1) %in% drops)])
OX_DF_1 <- as(OX_DF_1, "sparseMatrix")

OX_DF_2 <- as.data.frame(OX_data_2E)
OX_DF_2$CHID <- rownames(OX_DF_2)
OX_DF_2 <- merge(x = OX_DF_2, y = OL_CH, by.x = "CHID", by.y = "chirsutaOX", all.x = T)
rownames(OX_DF_2) <- OX_DF_2$athaliana_tair10
OX_DF_2 <- as.matrix(OX_DF_2[ ,!(names(OX_DF_2) %in% drops)])
OX_DF_2 <- as(OX_DF_2, "sparseMatrix")

OX_DF_3 <- as.data.frame(OX_data_3E)
OX_DF_3$CHID <- rownames(OX_DF_3)
OX_DF_3 <- merge(x = OX_DF_3, y = OL_CH, by.x = "CHID", by.y = "chirsutaOX", all.x = T)
rownames(OX_DF_3) <- OX_DF_3$athaliana_tair10
OX_DF_3 <- as.matrix(OX_DF_3[ ,!(names(OX_DF_3) %in% drops)])
OX_DF_3 <- as(OX_DF_3, "sparseMatrix")

OX_DF_7 <- as.data.frame(OX_data_7E)
OX_DF_7$CHID <- rownames(OX_DF_7)
OX_DF_7 <- merge(x = OX_DF_7, y = OL_CH, by.x = "CHID", by.y = "chirsutaOX", all.x = T)
rownames(OX_DF_7) <- OX_DF_7$athaliana_tair10
OX_DF_7 <- as.matrix(OX_DF_7[ ,!(names(OX_DF_7) %in% drops)])
OX_DF_7 <- as(OX_DF_7, "sparseMatrix")

### Now lets remove the hirsuta and thaliana ids from the data and assign the thaliana ids as row names

#############

dim(OX_DF_1)
OX_DF_1[10:15, 10:16]
OX_data_1E[10:15, 10:16]


dim(OX_DF_2)
OX_DF_2[10:15, 10:16]
OX_data_2E[10:15, 10:16]


dim(OX_DF_3)
OX_DF_3[10:15, 10:16]
OX_data_3E[10:15, 10:16]


dim(OX_DF_7)
OX_DF_7[10:15, 10:16]
OX_data_7E[10:15, 10:16]

##### The gene names are assigned correctly

rm(OX_data_1E)
rm(OX_data_2E)
rm(OX_data_3E)
rm(OX_data_7E)
gc()

#####

##### Remove the protoplasting induced genes
OX_DF_1 <- OX_DF_1[genesTOkeep, ]
OX_DF_2 <- OX_DF_2[genesTOkeep, ]
OX_DF_3 <- OX_DF_3[genesTOkeep, ]
OX_DF_7 <- OX_DF_7[genesTOkeep, ]


# Here, I have filtered out genes that are not detected in at least 21 cells - Done
OX_2E <- CreateSeuratObject(counts = OX_DF_2, project = "OX_2E", min.cells = 21, min.features = 200)
dim(OX_2E)
OX_2E <- subset(OX_2E, subset = nCount_RNA < 110000)
OX_2E
OX_W2 <- GetAssayData(OX_2E, assay = "RNA", slot = "counts")
colnames(OX_W2) <- paste("O2", colnames(OX_W2), sep = "_")

# Here, I have filtered out genes that are not detected in at least 18 cells - Done
OX_7E <- CreateSeuratObject(counts = OX_DF_7, project = "OX_7E", min.cells = 18, min.features = 200)
dim(OX_7E)
OX_7E <- subset(OX_7E, subset = nCount_RNA < 110000)
OX_7E
OX_W7 <- GetAssayData(OX_7E, assay = "RNA", slot = "counts")
colnames(OX_W7) <- paste("O7", colnames(OX_W7), sep = "_")

# Here, I have filtered out genes that are not detected in at least 13 cells - Done
OX_1E <- CreateSeuratObject(counts = OX_DF_1, project = "OX_1E", min.cells = 13, min.features = 200)
dim(OX_1E)
OX_1E <- subset(OX_1E, subset = nCount_RNA < 110000)
OX_1E
OX_W1 <- GetAssayData(OX_1E, assay = "RNA", slot = "counts")
colnames(OX_W1) <- paste("O1", colnames(OX_W1), sep = "_")

# Here, I have filtered out genes that are not detected in at least 8 cells - Done
OX_3E <- CreateSeuratObject(counts = OX_DF_3, project = "OX_3E", min.cells = 8, min.features = 200)
dim(OX_3E)
OX_3E <- subset(OX_3E, subset = nCount_RNA < 110000)
OX_3E
OX_W3 <- GetAssayData(OX_3E, assay = "RNA", slot = "counts")
colnames(OX_W3) <- paste("O3", colnames(OX_W3), sep = "_")

########## Integration of the datasets ##########

# Lets create the liger object
WT_Species <- createLiger(list(WC1 = COL_W1, WC3 = COL_W3, WC5 = COL_W5, WC2 = COL_W2, WC7 = COL_W7, WO2 = OX_W2, WO7 = OX_W7, WO1 = OX_W1, WO3 = OX_W3), remove.missing = F)

WT_Species <- normalize(WT_Species)

WT_Species <- selectGenes(WT_Species, num.genes = 2000)

WT_Species <- scaleNotCenter(WT_Species)

# Check which datasets are we integrating
table(WT_Species@cell.data$dataset)

WT_Species <- optimizeALS(WT_Species, k = 30, nrep = 30, lambda = 5)

save(WT_Species, file = "WT_Species_2000HVGs.Rdata")