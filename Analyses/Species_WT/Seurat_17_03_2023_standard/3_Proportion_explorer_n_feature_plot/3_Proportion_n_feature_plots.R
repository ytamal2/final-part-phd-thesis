# Load all the functions stored in scripts from the folder housing the scripts
scripts_list <- list.files("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions", pattern = "*.R$", full.names = TRUE) 
sapply(scripts_list, source, .GlobalEnv)

# Load
load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Seurat_08_03_2023/3_Integration_without_mincells_3000_HVGs/Integration/integrated_wt_species_seurat.RData")

# Storing directory
storing_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Seurat_08_03_2023/3_Integration_without_mincells_3000_HVGs"

cell_type_markers = read.csv("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Input_files/Additional_inputs/Known_markers_file/Curated_markers_thaliana.csv")

resolution_explorer(seuratObject = integrated.data, store_dir = storing_dir)
