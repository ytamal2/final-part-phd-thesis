
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/Load_libraries.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/load_rdata.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/genes_feature_plot.R")

str_extract(temp,"[[:digit:]]+")

regexp <- "[[:digit:]]+" 
str_extract(temp, regexp)

gsub(pattern = '.*-([0-9]+).*', replacement = "\\1", temp)

# This part go through all the elements of the vector and replaces the non-digit characters in the vector. But this is a bit confusing for me, although works perfect.
# To keep track between the Idents string, e.g. "RNA_snn_res.0.1", and folder naming
# let's go over the resolution values
# temp = names(integrated.data@meta.data)
temp = str_sort(names(integrated.data@meta.data)[str_detect(names(integrated.data@meta.data), pattern = "res")], numeric = TRUE)
#temp_res_dir = as.character(gsub("[^[:alnum:]]", "", temp))
temp_res_dir = substr(gsub("[^[:alnum:]]", "", temp), start = nchar(gsub("[^[:alnum:]]", "", temp)) - 1, stop = nchar(gsub("[^[:alnum:]]", "", temp)))
temp_res_dir[str_detect(string = temp_res_dir, pattern = "[A-Za-z]")] <- gsub(pattern = "[A-Za-z]",replacement = "", temp_res_dir[grep(pattern = "[A-Za-z]", x = temp_res_dir)])

# This script was created to test the functions and outputs


load("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects/seurat_object_of_K38.RData")

# Lets get the metadata file
md = integrated.data@meta.data

# Dataset information
integrated.data$Datasets <- integrated.data$orig.ident
integrated.data$Datasets <- factor(integrated.data$Datasets, levels = c("C1", "C2", "C3", "C5", "O1", "O2", "O3", "O7"), labels = c("COL-1", "COL-2", "COL-3", "COL-5", "OX-1", "OX-2", "OX-3", "OX-7"))

# Species information
integrated.data$Species <- integrated.data$orig.ident
integrated.data$Species <- substr(integrated.data$Species, 1, nchar(as.character(integrated.data$Species)) - 1)
integrated.data$Species <- factor(integrated.data$Species, levels = c("C", "O"), labels = c("Thaliana", "Hirsuta"))


############################ Change this chunk ############################
integrated.data$RNA_snn_res.0.2 <- factor(integrated.data$RNA_snn_res.0.2, levels = seq(0, length(levels(integrated.data$RNA_snn_res.0.2)) - 1))
Idents(integrated.data) <- integrated.data$RNA_snn_res.0.2

paste("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", "))

Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
###########################################################################

# UMAP - 2 components reduction
integrated.data <- RunUMAP(integrated.data, reduction = "inmf", dims = 1:dim(integrated.data@reductions[["inmf"]])[2], n.components = 2)
integrated.data <- RunTSNE(integrated.data, reduction = "inmf", dims = 1:dim(integrated.data@reductions[["inmf"]])[2], check_duplicates = FALSE, dim.embed = 2)

genes_feature_plot(seuratObject = integrated.data, store_dir = getwd(), genes_ID = c("AT1G62360", "AT5G67651"), genes_name = c("STM", "RCO"), reduction_name = "umap")







res_dir = getwd()

resolution_explorer(seuratObject = integrated.data, store_dir = getwd(), store_folder = "Factor_K_38")

# For a series of seurat object

# Lets get the saved file names
a = str_sort(list.files(path = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Species_WT/Liger_analysis_30_09_2022/Seurat_objects/", pattern = "seurat_object_of_"), numeric = TRUE)


replicate_proportion_per_cluster(seuratObject = integrated.data, store_dir = res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species")

species_proportion_per_cluster(seuratObject = integrated.data, store_dir = res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species", rep_prop = FALSE)

species_proportion_per_cluster(seuratObject = integrated.data, store_dir = res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species", rep_prop = TRUE)

feature_count_n_proportion(seuratObject = integrated.data, store_dir = res_dir, gene_ID = "AT1G62360", gene_name = "STM")

feature_count_n_proportion(seuratObject = integrated.data, store_dir = res_dir, gene_ID = "AT5G67651", gene_name = "RCO")

genes_feature_plot(seuratObject = integrated.data, store_dir = res_dir, genes_ID = c("AT1G62360", "AT5G67651"), genes_name = c("STM", "RCO"), reduction_name = "umap")

# let's go over the resolution values
# temp = names(integrated.data@meta.data)
temp = str_sort(names(integrated.data@meta.data)[str_detect(names(integrated.data@meta.data), pattern = "res")], numeric = TRUE)


for (i in c(1:length(temp))){
  Idents(integrated.data) <- temp[i]
  Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
  print(str_c("What is the active ident?", paste(levels(integrated.data@active.ident), collapse = ", ")))
}

as.character(gsub("[^[:alnum:]]", "", temp))

for (i in c(1:length(temp))){
  temp_str = substr(gsub("[^[:alnum:]]", "", temp[i]), start = nchar(gsub("[^[:alnum:]]", "", temp[i])) - 1, stop = nchar(gsub("[^[:alnum:]]", "", temp[i])))
  temp_str = str_c("RES_", gsub(pattern = "[A-Za-z]", replacement = "", temp_str))
  
  if (!dir.exists(temp_str)){
    dir.create(temp_str, showWarnings = TRUE, recursive = FALSE, mode = "0777")
  }
  temp_res_dir = str_c(temp_str, "/")
  
  # Set the cluster identity to the seurat object
  Idents(integrated.data) <- temp[i]
  Idents(integrated.data) <- factor(Idents(integrated.data), levels = seq(0, length(levels(Idents(integrated.data))) - 1))
  
  replicate_proportion_per_cluster(seuratObject = integrated.data, store_dir = temp_res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species")
  
  species_proportion_per_cluster(seuratObject = integrated.data, store_dir = temp_res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species", rep_prop = FALSE)
  
  species_proportion_per_cluster(seuratObject = integrated.data, store_dir = temp_res_dir, replicate_metadata_name = "Datasets", species_metadata_name = "Species", rep_prop = TRUE)
  
  feature_count_n_proportion(seuratObject = integrated.data, store_dir = temp_res_dir, gene_ID = "AT1G62360", gene_name = "STM")
  
  feature_count_n_proportion(seuratObject = integrated.data, store_dir = temp_res_dir, gene_ID = "AT5G67651", gene_name = "RCO")
  
  genes_feature_plot(seuratObject = integrated.data, store_dir = temp_res_dir, genes_ID = c("AT1G62360", "AT5G67651"), genes_name = c("STM", "RCO"), reduction_name = "umap")
}
