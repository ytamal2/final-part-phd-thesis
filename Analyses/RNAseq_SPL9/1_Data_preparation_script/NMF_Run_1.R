###
# Load all the packages
###

library("NMF")

###
# Load data
###

load("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Analyses/RNAseq_SPL9/1_Data_preparation_script/norm_all_data.RData")

###
# Run NMF
###

for (i in c(2:20)){
  classifier = nmf(norm_all_data, rank = i, 'Brunet', seed = 42, .opt = 'vP10', .pbackend = 10)
  save(classifier, file = str_c("spl9_K_", i, ".RData"))
}

###
# Load the session information
###

writeLines(capture.output(sessionInfo()), "Session_info_spl9_nmf_1.txt")
