source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/Load_libraries.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/load_rdata.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/clus_tree_of_cell_clusters.R")

# Load the known cell type markers file
known_markers = read.csv("/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Input_files/Additional_inputs/Known_markers_file/Thaliana_known_cell_type_markers.csv")

# Storing directory
storing_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Hirsuta_n_APEX"

clus_tree_of_cell_clusters(seuratObject = integrated.data, marker_file = known_markers, query_pattern = "RNA_snn_res.", gene_ID_column = "AT_ID", gene_name_column = "Gene_Name", store_dir = storing_dir)
