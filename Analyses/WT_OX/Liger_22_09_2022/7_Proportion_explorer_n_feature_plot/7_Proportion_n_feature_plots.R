source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/resolution_explorer.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/resolution_explorer_series_of_seurat_objects.R")

# Directory containing the seurat objects
seurat_files_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Hirsuta_n_APEX/Seurat_objects/"

storing_dir = "/netscratch/dep_tsiantis/grp_laurent/tamal/2022/Analyses/Hirsuta_n_APEX"

resolution_explorer_series_of_seurat_objects(seurat_object_dir = seurat_files_dir, file_name_pattern = "seurat_object_of_", store_dir = storing_dir, store_folder = "Proportion_and_features_plot")


