source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/Load_libraries.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/RDimension_plot.R")
source("/home/ytamal2/Documents/2022/Final_part_PhD_Thesis/Functions/genes_feature_plot.R")

# Load the saved seurat object
load("/netscratch/dep_tsiantis/common/yasir/WT_Cardamine/Integrated_seurat_object/integrated_ox_wt_seurat.RData")

# integrated.data <- RunUMAP(integrated.data, reduction = "pca", dims = 1:50, n.components = 2, n.neighbors = 50, seed.use = 35)
# 
# save(integrated.data, file = "/netscratch/dep_tsiantis/common/yasir/WT_Cardamine/Integrated_seurat_object/integrated_ox_wt_seurat.RData")

DefaultAssay(integrated.data) <- "RNA"

RDimension_plot(seuratObject = integrated.data, store_dir = "/netscratch/dep_tsiantis/common/yasir/WT_Cardamine/Integrated_seurat_object", store_folder = "UMAP_plot",dimension_reduction_name = "umap")

genes_feature_plot(seuratObject = integrated.data, store_dir = "/netscratch/dep_tsiantis/common/yasir/WT_Cardamine/Integrated_seurat_object", genes_ID = c("Chir02Ox-b03100.2", "Chir06Ox-b35150.2"), genes_name = c("STM", "RCO"), reduction_name = "umap", store_folder = "Genes_expression")
