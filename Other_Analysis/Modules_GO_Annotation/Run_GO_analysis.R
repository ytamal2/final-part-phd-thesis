source("/home/ytamal2/Documents/My_Funcs/GO_annotation/GO_annotation_of_genes.R")

# Lets store the directory path where we are going to save ou results in a variable
res_dir = "/netscratch/dep_tsiantis/grp_laurent/common/naik"

# let's load the modules
load("/netscratch/dep_tsiantis/grp_laurent/common/naik/module-list.RData")


# List of all thaliana genes
ATgenes <- read.delim("/netscratch/dep_tsiantis/grp_laurent/common/tamal/ATgenes.txt", header = FALSE, col.names = "genes")
ATgenes <- as.character(ATgenes$genes)

# GO annotation
markers_GO_annotation(marker_set = ll, 
                      species_gene_set = ATgenes, 
                      store_dir = res_dir, 
                      store_folder = "GO_annotation_results", 
                      GO_results_folder_name = "Modules_GO_Annotation")




